<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\Genre;
use Illuminate\Support\Facades\File;

class FilmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }

    // protected $fillable= ['judul','ringkasan','tahun','poster','genre_id'];
    public function create()
    {
        $genre = Genre::all();
        return view('film.tambah',compact('genre'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|mimes:jpg,jpeg,png|max:2048',
            'genre_id' => 'required'
        ]);

        $posterName = time().'.'.$request->poster->extension();
        $request->poster->move(public_path('image'),$posterName);

        $film = new Film;
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $posterName;
        $film->genre_id = $request->genre_id;

        $film->save();

        return redirect('/film');
    }

    public function index()
    {
        $film = Film::all();
        return view('film.index',compact('film'));
    }

    public function show($id)
    {
        $filmDetail = Film::find($id);
        return view('film.detail',compact('filmDetail'));
    }

    public function edit($id)
    {
        $genre = Genre::all();
        $filmDetail = Film::find($id);
        return view('film.edit',compact('filmDetail','genre'));
    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'mimes:jpg,jpeg,png|max:2048',
            'genre_id' => 'required'
        ]);

        $film = Film::find($id);
        if($request->has('poster')){
            $path = "image/";
            File::delete($path . $film->poster);

            $posterName = time().'.'.$request->poster->extension();
            $request->poster->move(public_path('image'),$posterName);
            $film->poster = $posterName;
            $film->save();

            return redirect('/film');
        }

        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->genre_id = $request->genre_id;

        $film->save();

        return redirect('/film');
    }

    public function destroy($id)
    {
        $path = "image/";
        $film = Film::find($id);

        File::delete($path . $film->poster);

        $film->kritik()->delete();
        $film->delete();

        return redirect('/film');
    }
}
