<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kritik;
use Illuminate\Support\Facades\Auth;

class KritikController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function store(Request $request)
    {
        $request->validate([
            'point' => 'required',
            'content' => 'required'
        ]);

        $kritik = new Kritik();
        $userId = Auth::id();

        $kritik->point = $request->point;
        $kritik->content = $request->content;
        $kritik->user_id = $userId;
        $kritik->film_id = $request->film_id;

        $kritik->save();

        return redirect('/film/'.$request->film_id);
    }
}
