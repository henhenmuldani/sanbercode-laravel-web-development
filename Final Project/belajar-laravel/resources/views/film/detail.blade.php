@extends('layout.master')

@section('judul')
Halaman Film Detail
@endsection

@section('content')

<h1>{{$filmDetail->nama}}</h1>
<div class="card">
    <img class="img-top" src="{{asset('image/'.$filmDetail->poster)}}" alt="card image cap">
    <div class="card-body">
        <h2 class="card-title">{{$filmDetail->judul}}</h2>
        <p class="card-text">{{$filmDetail->tahun}} <span class="badge badge-primary">{{$filmDetail->genre->nama}}</span>
        </p>
        <p class="card-text">{{$filmDetail->ringkasan}}</p>
    </div>
</div>

<h1>List Kritik</h1>
{{-- list kritik --}}
@forelse ($filmDetail->kritik as $item)
<div class="card">
    <div class="card-header">{{$item->user->name}}</div>
    <div class="card-body">
        <h2 class="text-warning">Point {{$item->point}}/5</h2>
        <p class="card-text">{{$item->content}}</p>
    </div>
</div>
@empty
    <h2>Belum ada Kritik</h2>
@endforelse

@auth
   {{-- form kritik --}}
<div>
    <form action="/kritik" method="POST" class="mt-4">
        @csrf
        <div class="form-group">
            <select name="point" class="form-control" id="">
                <option value="">--Pilih Point yang diberikan--</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
        </div>
        @error('point')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <input type="hidden" value="{{$filmDetail->id}}" name="film_id">
            <textarea name="content" cols="30" rows="10" placeholder="isi kritik" class="form-control"></textarea>
        </div>
        @error('content')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Post Kritik</button>


    </form>
</div>
@endauth


<a href="/film" class="btn btn-primary btn-sm my-2">Kembali</a>

@endsection

