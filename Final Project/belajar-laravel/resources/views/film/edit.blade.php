@extends('layout.master')

@section('judul')
Halaman Edit Film
@endsection

@section('content')
<form action="/film/{{$filmDetail->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Judul Film</label>
        <input type="text" value="{{$filmDetail->judul}}" name="judul" class="form-control">
      </div>
      @error('judul')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
          <label>Ringkasan Film</label>
          <textarea name="ringkasan" class="form-control" cols="30" rows="10">{{old('ringkasan',$filmDetail->ringkasan)}}</textarea>
        </div>
        @error('ringkasan')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label>Tahun</label>
          <input type="number" value="{{$filmDetail->tahun}}" name="tahun" class="form-control">
        </div>
        @error('tahun')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <label>Poster Film</label>
          <input type="file" value="{{$filmDetail->poster}}" name="poster" class="form-control">
        </div>
        @error('poster')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <label>Genre Film</label>
          <select  name="genre_id" id="" class="form-control">
              <option value="">--Pilih Genre--</option>
              @forelse ($genre as $item)
                @if ($item->id === $filmDetail->genre_id )
                 <option value="{{$item->id}}" selected>{{$item->nama}}</option>

                @else
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endif
              @empty
                  <option value="">Tidak ada genre</option>
              @endforelse
          </select>
        </div>
        @error('genre_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection

