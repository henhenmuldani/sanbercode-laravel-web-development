@extends('layout.master')

@section('judul')
Halaman Film
@endsection

@section('content')

@auth
<a href="/film/create" class="btn btn-primary btn-sm my-2">Tambah Film</a>

@endauth

<div class="row">
    @forelse ($film as $key => $item)
        <div class="col-4">
            <div class="card">
                <img class="img-top" src="{{asset('image/'.$item->poster)}}" alt="card image cap">
                <div class="card-body">
                    <h2 class="card-title">{{$item->judul}}</h2>
                    <p class="card-text">{{$item->tahun}} <span class="badge badge-primary">{{$item->genre->nama}}</span>
                    </p>
                    <p class="card-text">{{Str::limit($item->ringkasan, 20)}}</p>

                    @auth
                    <form action="/film/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm my-2">Detail</a>
                        <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm my-2">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                    @endauth

                    @guest
                        <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm my-2">Detail</a>
                    @endguest

                </div>
            </div>
        </div>
@empty
    <h1>Data Film Kosong</h1>
@endforelse
</div>

@endsection

