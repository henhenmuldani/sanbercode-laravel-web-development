@extends('layout.master')

@section('judul')
Halaman Genre Detail
@endsection

@section('content')
<h1>{{$genreDetail->nama}}</h1>
<div class="row">
    @forelse ($genreDetail->film as $item)
        <div class="col-4">
            <div class="card">
                <img class="img-top" src="{{asset('image/'.$item->poster)}}" alt="card image cap">
                <div class="card-body">
                    <h2 class="card-title">{{$item->judul}}</h2>
                    <p class="card-text">{{$item->tahun}}</p>
                    <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm my-2">Read More</a>

                </div>
            </div>
        </div>
    @empty
        <h2>Tidak ada berita pada kategori ini</h2>
    @endforelse
</div>
<a href="/genre" class="btn btn-primary btn-sm my-2">Kembali</a>

@endsection

