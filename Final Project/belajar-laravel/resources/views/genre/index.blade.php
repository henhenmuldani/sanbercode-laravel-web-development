@extends('layout.master')

@section('judul')
Halaman Genre
@endsection

@section('content')

@auth
<a href="/genre/create" class="btn btn-primary btn-sm my-2">Tambah Genre</a>

@endauth
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->nama}}</td>
                <td>

                    @auth
                    <form action="/genre/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/genre/{{$item->id}}" class="btn btn-primary btn-sm my-2">Detail</a>
                        <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm my-2">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                    @endauth

                    @guest
                    <a href="/genre/{{$item->id}}" class="btn btn-primary btn-sm my-2">Detail</a>
                    @endguest


                </td>
            </tr>
        @empty
            <h1>Data Genre Kosong</h1>
        @endforelse
    </tbody>
  </table>
@endsection

