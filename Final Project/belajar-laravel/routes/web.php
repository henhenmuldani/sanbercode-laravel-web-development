<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\KritikController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class,'index']);
Route::get('/register', [AuthController::class,'index']);
Route::post('/welcome', [AuthController::class,'welcome']);
Route::get('/data-table', [IndexController::class,'table']);

// Route::middleware(['auth'])->group(function(){

// });

    //CRUD Cast
    //Create dan Store
    Route::get('/cast/create', [CastController::class,'create']);
    Route::post('/cast', [CastController::class,'store']);
    //Index
    Route::get('/cast', [CastController::class,'index']);
    //Show
    Route::get('/cast/{cast_id}', [CastController::class,'show']);
    //Edit
    Route::get('/cast/{cast_id}/edit', [CastController::class,'edit']);
    //Update
    Route::put('/cast/{cast_id}', [CastController::class,'update']);
    //Delete
    Route::delete('/cast/{cast_id}', [CastController::class,'destroy']);

     //CRUD Genre
    //Create dan Store
    Route::get('/genre/create', [GenreController::class,'create']);
    Route::post('/genre', [GenreController::class,'store']);
    //Index
    Route::get('/genre', [GenreController::class,'index']);
    //Show
    Route::get('/genre/{genre_id}', [GenreController::class,'show']);
    //Edit
    Route::get('/genre/{genre_id}/edit', [GenreController::class,'edit']);
    //Update
    Route::put('/genre/{genre_id}', [GenreController::class,'update']);
    //Delete
    Route::delete('/genre/{genre_id}', [GenreController::class,'destroy']);

    //CRUD Film
    //Create dan Store
    Route::get('/film/create', [FilmController::class,'create']);
    Route::post('/film', [FilmController::class,'store']);
    //Index
    Route::get('/film', [FilmController::class,'index']);
    //Show
    Route::get('/film/{film_id}', [FilmController::class,'show']);
    //Edit
    Route::get('/film/{film_id}/edit', [FilmController::class,'edit']);
    //Update
    Route::put('/film/{film_id}', [FilmController::class,'update']);
    //Delete
    Route::delete('/film/{film_id}', [FilmController::class,'destroy']);

    //kritik
    Route::post('/kritik', [KritikController::class,'store']);

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');






/*
/cast/{cast_id} 	GET 	CastController@show
/cast/{cast_id}/edit 	GET 	CastController@edit
/cast/{cast_id} 	PUT 	CastController@update
/cast/{cast_id} 	DELETE 	CastController@destroy  */

// Route::get('/master', function(){
//     return view('layout.master');
// });



