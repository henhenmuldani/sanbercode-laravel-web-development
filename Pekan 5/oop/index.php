<?php
require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");
echo "Name : ".$sheep->name."<br/>"; // "shaun"
echo "Legs : ".$sheep->legs."<br/>"; // 2
if($sheep->cold_blooded === true){
    echo "Cold Blooded : Yes"."<br/>";
}else{
    echo "Cold Blooded : No"."<br/>";
}//false
echo "<br/>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$kodok = new Frog("buduk");
echo "Name : ".$kodok->name."<br/>"; // "buduk"
echo "Legs : ".$kodok->legs."<br/>"; // 4
if($kodok->cold_blooded === true){
    echo "Cold Blooded : Yes"."<br/>";
}else{
    echo "Cold Blooded : No"."<br/>";
}//false
echo "Jump : ".$kodok->jump()."<br/><br/>"; // "hop hop"
//echo $bool_val ? 'true' : 'false';

$sungokong = new Ape("kera sakti");
echo "Name : ".$sungokong->name."<br/>"; // "kera sakti"
echo "Legs : ".$sungokong->legs."<br/>"; // 2
if($sungokong->cold_blooded === true){
    echo "Cold Blooded : Yes"."<br/>";
}else{
    echo "Cold Blooded : No"."<br/>";
}//false
echo "Yell : ".$sungokong->yell()."<br/><br/>"; // "Auooo"


?>