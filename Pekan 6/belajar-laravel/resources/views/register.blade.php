@extends('layout.master')

@section('judul')
Form
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>
<form action="/welcome" method="post">
    @csrf
  <label>First Name:</label><br /><br />
  <input type="text" name="firstName" /><br /><br />
  <label>Last Name:</label><br /><br />
  <input type="text" name="lastName" /><br /><br />
  <label>Gender:</label><br /><br />
  <input type="radio" name="gender" value="1" /> Male <br />
  <input type="radio" name="gender" value="2" /> Female<br />
  <input type="radio" name="gender" value="3" /> Other<br /><br />
  <label>Nationality:</label><br /><br />
  <select name="nationality">
    <option value="1">Indonesian</option>
    <option value="2">Singaporian</option>
    <option value="3">Malaysian</option>
    <option value="4">Australian</option></select
  ><br /><br />
  <label>Language Spoken:</label><br /><br />
  <input type="checkbox" name="language" value="1" /> Bahasa Indonesia<br />
  <input type="checkbox" name="language" value="2" /> English<br />
  <input type="checkbox" name="language" value="3" /> Other<br /><br />
  <label>Bio:</label><br /><br />
  <textarea name="bio" id="" cols="30" rows="10"></textarea><br />
  <input type="submit" value="Sign Up" />
</form>
@endsection

