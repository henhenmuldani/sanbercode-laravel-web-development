<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
        //dd($request->all());

        $namaDepan = $request->input('firstName');
        $namaBelakang = $request->input('lastName');

        return view('/welcome',compact('namaDepan','namaBelakang'));

        //bisa diringkas
        //$data = $request->all();
        //return view('/welcome',compact('data'));
        //nanti panggil di welcome
        // <h1>SELAMAT DATANG {{$data['firstName']}}</h1>

    }
}
