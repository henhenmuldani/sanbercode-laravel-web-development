@extends('layout.master')

@section('judul')
Halaman Cast Detail
@endsection

@section('content')
<h1>{{$castDetail->nama}}</h1>
<p>{{$castDetail->umur}}</p>
<p>{{$castDetail->bio}}</p>
<a href="/cast" class="btn btn-primary btn-sm my-2">Kembali</a>

@endsection

