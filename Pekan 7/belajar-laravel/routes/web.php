<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class,'index']);
Route::get('/register', [AuthController::class,'index']);
Route::post('/welcome', [AuthController::class,'welcome']);
Route::get('/data-table', [IndexController::class,'table']);

//CRUD Cast
//Create dan Store
Route::get('/cast/create', [CastController::class,'create']);
Route::post('/cast', [CastController::class,'store']);
//Index
Route::get('/cast', [CastController::class,'index']);
//Show
Route::get('/cast/{cast_id}', [CastController::class,'show']);
//Edit
Route::get('/cast/{cast_id}/edit', [CastController::class,'edit']);
//Update
Route::put('/cast/{cast_id}', [CastController::class,'update']);
//Delete
Route::delete('/cast/{cast_id}', [CastController::class,'destroy']);






/*
/cast/{cast_id} 	GET 	CastController@show
/cast/{cast_id}/edit 	GET 	CastController@edit
/cast/{cast_id} 	PUT 	CastController@update
/cast/{cast_id} 	DELETE 	CastController@destroy  */

// Route::get('/master', function(){
//     return view('layout.master');
// });


