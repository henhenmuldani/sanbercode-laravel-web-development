//stepbystep
1. buka php myadmin
2. bikin database laravel_film
3. buka .env
4. Tambahkan database_name = laravel_film
5. php artisan migrate
6. php artisan migrate:status //untuk cek
7. buat dulu migrate user, cast, genre yg tidak ada forign key
8. habis itu buat forign key di table yg kita buat
9. buat profile, film
10. lalu buat peran dan juga kritik
11. table user boleh pakai bawaan laravel
12. php artisan make:migration create_cast_table
13.         Schema::create('cast', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('umur');
            $table->string('bio');
            $table->timestamps();
        });
14. php artisan make:migration create_genre_table
15. php artisan migrate
16. buat foreign key untuk profile, film
17. php artisan make:migration create_profile_table
18.             $table->id();
            $table->integer('umur');
            $table->text('bio');
            $table->text('alamat');
            //foreign key
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
19. php artisan make:migration create_film_table
20.             $table->id();
            $table->string('judul');
            $table->text('ringkasan');
            $table->integer('tahun');
            $table->string('poster');
            //foreign key
            $table->unsignedBigInteger('genre_id');
            $table->foreign('genre_id')->references('id')->on('genre');
            $table->timestamps();
21. php artisan migrate
22. php artisan migrate:status
23. jika ingin menambah kolom di tabel maka gunakan ...
24. php artisan make:migration add_penulis_to_berita_table --table-berita
25. masukkan di function up untuk tambah kolom  $table->string('penulis');
26. masukkan di down untuk hapus kolom $table->dropColumn('penulis');
27. buat migrate peran dan kritik
28. php artisan make:migration create_peran_table
29. $table->id();
            //foreign key
            $table->unsignedBigInteger('film_id');
            $table->foreign('film_id')->references('id')->on('film');
            //foreign key
            $table->unsignedBigInteger('cast_id');
            $table->foreign('cast_id')->references('id')->on('cast');
            $table->string('nama');
            $table->timestamps();
30. php artisan make:migration create_kritik_table
31.             $table->id();
            //foreign key
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            //foreign key
            $table->unsignedBigInteger('film_id');
            $table->foreign('film_id')->references('id')->on('film');
            $table->text('content');
            $table->integer('point');
            $table->timestamps();
32. buat crud untuk cast
33. php artisan make:model Cast
34. dokumentasi https://laravel.com/docs/9.x/eloquent
35. stelah buat model buka app/Http/Models/Cast.php
36. buat controllernya
37. php artisan make:controller CastController
38. buat route untuk Cast di web.php
39. buka CastController
40. tulis fungsi create 
41. return ke view/cast/tambah.blade.php
42. buat file tambah.blade.php di resources/view/tambah
43. tambahkan format form dari bootstrap, pilih yg sesuai
44. buat fungsi store
45. 


Release 1
Buat Database Menggunakan Migration Laravel
Catatan : Tugas migration ini boleh disimpan di project Laravel tugas kemarin (melanjutkan).
Berikut ini adalah gambaran dari database yang akan dibuat menggunakan migration :
Catatan:
Nama table dan nama column boleh berbeda, tidak mesti sama dengan yang di contoh.
untuk table user bisa menggunakan migration bawaan laravel
NB: file migration bawaan laravel jangan di hapus karena akan digunakan di materi laravel auth

Release 2
Siapkan Database Dengan Migration
Melanjutkan dari release 1, pastikan database sudah siap dengan melakukan migration 
untuk tabel-tabel yang diperlukan.
Untuk saat ini, tabel yang diperlukan hanya tabel cast saja.
2. Proses CRUD
Buat modelnya php artisan make:model Cast
Setelah table tersedia, maka kita dapat mulai membuat proses CRUD. Buatlah CRUD untuk fitur Cast. Berikut ini gambaran Route dan Controller yang diinginkan
url 	Methods 	handler 	Keterangan
/cast 	GET 	CastController@index 	menampilkan list data para pemain film (boleh menggunakan table html atau bootstrap card)
/cast/create 	GET 	CastController@create 	menampilkan form untuk membuat data pemain film baru
/cast 	POST 	CastController@store 	menyimpan data baru ke tabel Cast
/cast/{cast_id} 	GET 	CastController@show 	menampilkan detail data pemain film dengan id tertentu
/cast/{cast_id}/edit 	GET 	CastController@edit 	menampilkan form untuk edit pemain film dengan id tertentu
/cast/{cast_id} 	PUT 	CastController@update 	menyimpan perubahan data pemain film (update) untuk id tertentu
/cast/{cast_id} 	DELETE 	CastController@destroy 	menghapus data pemain film dengan id tertentu